package eg.edu.alexu.csd.oop.calculator.cs74;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ICalculator implements Calculator {
	double[] arr = new double[2];
	String operator;
	ArrayList<String[]> data = new ArrayList<String[]>();
	int dataIndex = -1;
	int prevIndex = 0;

	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		if (!s.matches("^-?\\d+(\\.\\d+)?(\\s+)?[-+*\\/](\\s+)?-?\\d+(\\.\\d+)?(\\s+)?$")) {
			operator = "error";

		} else {
			s = s.replaceAll("\\s+", "");

			String[] arrOfStr = s.split("[-+*\\/]");
			String[] arrOfStr1 = s.split("\\d+(\\.\\d+)?");
			operator = arrOfStr1[1].charAt(0) + "";
			int index = 0;
			for (String a : arrOfStr) {
				if (!a.isEmpty()) {
					if (index == 0 && s.charAt(0) == '-') {
						a = "-" + a;
						arr[index] = Double.parseDouble(a);
					} else if (index == 1 && (arrOfStr[1].equals("") || arrOfStr.length == 4)) {
						a = "-" + a;
						arr[index] = Double.parseDouble(a);
					}

					else {
						arr[index] = Double.parseDouble(a);

					}
					index++;
				}
			}
			index = 0;
		}

	}

	@Override
	public String getResult() {
		// TODO Auto-generated method stub
		if(operator==null) {
			return "empty" ;
		}
		if (operator.equals("error")) {
			return "invalid input";
		}
		double answer = 0;
		;
		double given1 = arr[0];

		double given2 = arr[1];
		if (operator.equals("+")) {
			answer = given1 + given2;
		} else if (operator.equals("-")) {
			answer = given1 - given2;
		} else if (operator.equals("*")) {
			answer = given1 * given2;
		} else if (operator.equals("/")) {
			if (given2 == 0) {
				answer = Integer.MIN_VALUE;

			} else {
				answer = given1 / given2;
			}
		}
		if (answer == Integer.MIN_VALUE) {
			return "can't divide be zero";
		}
		String[] arr1 = new String[2];
		arr1[0] = given1 + operator + given2;
		arr1[1] = Double.toString(answer);
		dataIndex++;

		dataIndex = dataIndex % 5;
		prevIndex = dataIndex;
		if (data.size() == 5) {
			for (int i = 0; i < 4; i++) {
				data.set(i, data.get(i + 1));
			}
			data.set(4, arr1);
			prevIndex = 4;
		} else {
			data.add(dataIndex, arr1);
//			System.out.println(prevIndex);
		}

		return Double.toString(answer);

	}

	@Override
	public String current() {
		// TODO Auto-generated method stub
		if(data.size()==0) {
			return "history is empty";
		}
		return data.get(prevIndex)[0];
	}

	@Override
	public String prev() {
		String answer = null;

		prevIndex = prevIndex - 1;
		if (prevIndex >= 0) {
			answer = data.get(prevIndex)[1];
		} else {
			prevIndex++;
		}

		return answer;
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		String answer = null;
		prevIndex = prevIndex + 1;

		if (prevIndex < data.size() && prevIndex >= 0) {
			answer = data.get(prevIndex)[1];

		} else {
			prevIndex--;
		}

		return answer;
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub

		FileWriter writer;
		try {
			writer = new FileWriter("save.txt");
			for (int i = 0; i < data.size(); i++) {
				writer.write(data.get(i)[0]);
				writer.write("\n");
			}
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		try {
			FileReader read = new FileReader("save.txt");
			int i;
			try {
				String s = "";
				String[] s1 = new String[2];
				data.clear();
				dataIndex = -1;
				int index=0;
				if(!read.toString().isEmpty()) {
				while ((i = read.read()) != -1) {
					if(index!=0&&i=='\n') s1[1]=getResult() ;
				
					if (i != '\n') {
						s = s + (char) i;
					} else {
						index++;
						input(s);
						s1[0] = s;
						s = "";
					}
				}
				prevIndex = data.size() ;
			}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
