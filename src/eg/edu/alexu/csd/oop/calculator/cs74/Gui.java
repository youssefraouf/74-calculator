package eg.edu.alexu.csd.oop.calculator.cs74;
import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JTextField;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Gui {
	Calculator c = new ICalculator();
	private JFrame frame;
	private JTextField input1;
	private JButton next;
	private JButton previous;
	private JButton save;
	private JButton load;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);
		
				input1 = new JTextField();
				GridBagConstraints gbc_input1 = new GridBagConstraints();
				gbc_input1.insets = new Insets(0, 0, 5, 5);
				gbc_input1.gridx = 1;
				gbc_input1.gridy = 1;
				frame.getContentPane().add(input1, gbc_input1);
				input1.setColumns(10);

		previous = new JButton("Previous");

		GridBagConstraints gbc_previous = new GridBagConstraints();
		gbc_previous.insets = new Insets(0, 0, 5, 0);
		gbc_previous.gridx = 2;
		gbc_previous.gridy = 2;
		frame.getContentPane().add(previous, gbc_previous);

		JButton calc = new JButton("Calculate");
		calc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		GridBagConstraints gbc_calc = new GridBagConstraints();
		gbc_calc.insets = new Insets(0, 0, 5, 5);
		gbc_calc.gridx = 1;
		gbc_calc.gridy = 3;
		frame.getContentPane().add(calc, gbc_calc);

		next = new JButton("Next");
		next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_next = new GridBagConstraints();
		gbc_next.insets = new Insets(0, 0, 5, 0);
		gbc_next.gridx = 2;
		gbc_next.gridy = 4;
		frame.getContentPane().add(next, gbc_next);

		JLabel result = new JLabel("Result");
		GridBagConstraints gbc_result = new GridBagConstraints();
		gbc_result.insets = new Insets(0, 0, 5, 5);
		gbc_result.gridx = 1;
		gbc_result.gridy = 5;
		frame.getContentPane().add(result, gbc_result);
		
		save = new JButton("save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_save = new GridBagConstraints();
		gbc_save.insets = new Insets(0, 0, 0, 5);
		gbc_save.gridx = 0;
		gbc_save.gridy = 8;
		frame.getContentPane().add(save, gbc_save);
		
		load = new JButton("load");
		GridBagConstraints gbc_load = new GridBagConstraints();
		gbc_load.gridx = 2;
		gbc_load.gridy = 8;
		frame.getContentPane().add(load, gbc_load);
		calc.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				c.input(input1.getText());
				result.setText(c.getResult());
			}
		});

		previous.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			String s = c.prev() ;
//			System.out.println(s);
				if(s!=null) {
				result.setText(s);
				input1.setText(c.current());
				}
			}
		});
		next.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			String s = c.next() ;
				if(s!=null) {
				result.setText(s);
				input1.setText(c.current());
				}
			}
		});
		save.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
		c.save();
			}
		});
		load.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
		c.load();
		result.setText(c.getResult());
		input1.setText(c.current());
		
			}
		});
	}

}
